/*
	dstapply
	paleondires
	
	AITF Gateway Client
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <linux/types.h>
#include <linux/netfilter.h>		/* for NF_ACCEPT */
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <string.h>
#include "gateway.h"

#include <libnetfilter_queue/libnetfilter_queue.h>

#define NUMHOSTS 3

struct Host_Info hostTable[NUMHOSTS];
struct Shim_Header currentShim;

const char* ipAddress = "169.254.5.139";
/* returns packet id */
static u_int32_t print_pkt (struct nfq_data *tb)
{
	int id = 0;
	struct nfqnl_msg_packet_hdr *ph;
	struct nfqnl_msg_packet_hw *hwph;
	u_int32_t mark,ifi; 
	int ret;
	unsigned char *data;
	uint32_t ip_src, ip_dest;
	struct in_addr s_ip, d_ip;

	ph = nfq_get_msg_packet_hdr(tb);
	if (ph) {
		id = ntohl(ph->packet_id);
		printf("hw_protocol=0x%04x hook=%u id=%u ",
			ntohs(ph->hw_protocol), ph->hook, id);
	}

	hwph = nfq_get_packet_hw(tb);
	if (hwph) {
		int i, hlen = ntohs(hwph->hw_addrlen);

		printf("hw_src_addr=");
		for (i = 0; i < hlen-1; i++)
			printf("%02x:", hwph->hw_addr[i]);
		printf("%02x ", hwph->hw_addr[hlen-1]);
	}

	mark = nfq_get_nfmark(tb);
	if (mark)
		printf("mark=%u ", mark);

	ifi = nfq_get_indev(tb);
	if (ifi)
		printf("indev=%u ", ifi);

	ifi = nfq_get_outdev(tb);
	if (ifi)
		printf("outdev=%u ", ifi);
	ifi = nfq_get_physindev(tb);
	if (ifi)
		printf("physindev=%u ", ifi);

	ifi = nfq_get_physoutdev(tb);
	if (ifi)
		printf("physoutdev=%u ", ifi);

	ret = nfq_get_payload(tb, &data);
	if (ret >= 0) {
		printf("payload_len=%d ", ret);
		ip_src = *((uint32_t*) (data + 12));
		s_ip.s_addr = (uint32_t) ip_src;
		printf(" source IP %s", inet_ntoa(s_ip));
		ip_dest = *((uint32_t*) (data + 16));
		d_ip.s_addr = (uint32_t) ip_dest;
		printf(" destination IP %s\n", inet_ntoa(d_ip));
	}
	
	fputc('\n', stdout);

	return id;
}
	

static int cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg,
	      struct nfq_data *nfa, void *data)
{
	u_int32_t id = print_pkt(nfa);
	printf("entering callback\n");
	
	struct Shim_Header sh;
	struct nfqnl_msg_packet_hdr *ph;
	unsigned char *buffer;
	uint32_t ip_src, ip_dest;
	struct in_addr s_ip, d_ip;
	uint8_t TTL;
	bool fromHost = false;
	bool toLegacyHost = false;
	unsigned char* buf = NULL;
	void* bufferpointer;
	void* bufferpointer2;
	int ipHeaderOffset = 20;
	
	ph = nfq_get_msg_packet_hdr(nfa);
	
	int ret = nfq_get_payload(nfa, &buffer);
	if (ret >= 0) {
		memcpy(&TTL, (buffer + 8), 1);
		printf("TTL = %i\n", TTL);
		ip_src = *((uint32_t*) (buffer + 12));
		s_ip.s_addr = (uint32_t) ip_src;
		ip_dest = *((uint32_t*) (buffer + 16));
		d_ip.s_addr = (uint32_t) ip_dest;
		for (int i = 0; i < NUMHOSTS; i++) {
			printf("hostTable[%i].ip: %s ", i, hostTable[i].ip);
			printf("source address: %s\n", inet_ntoa(s_ip));
			if (!strcmp(hostTable[i].ip, inet_ntoa(s_ip))) {
				printf("Packet from host.\n");
				fromHost = true;
				// Dont have to check other hosts
				break;
			} else if (!strcmp(hostTable[i].ip, inet_ntoa(d_ip)) && hostTable[i].isLegacy) {
				printf("Packet from legacy host");
				toLegacyHost = true;
				// Dont have to check other hosts
				break;
			}
		}
		
		// avoid editing SYN packets (TCP Handshaking)
		if (ret > 76) {
			// Gateway
			if (TTL < 63) {
				///////////////////////
				// Extract Shim header
				
				// Read shim header
				printf("///////////////////////////////////\n");
				printf("Reading shim header...\n");
		
				printf("Shim Header Info\n");
				sh.type = *((uint8_t*) (buffer + ipHeaderOffset));
				printf("sh.type = %i\n", sh.type);
				sh.size = *((uint8_t*) (buffer + ipHeaderOffset + 1));
				printf("sh.size = %i\n", sh.size);
				sh.offset = *((uint16_t*) (buffer + ipHeaderOffset + 2));
				printf("sh.offset = %i\n", sh.offset);
				sh.nonce = *((uint32_t*) (buffer + ipHeaderOffset + 4));
				printf("sh.nonce = %i\n", sh.nonce);
				sh.ipaddr1 = *((uint32_t*) (buffer + ipHeaderOffset + 8));
				printf("sh.ipaddr1 = %i\n", sh.ipaddr1);
				sh.r1 = *((uint32_t*) (buffer + ipHeaderOffset + 12));
				printf("sh.r1 = %i\n", sh.r1);
				sh.ipaddr2 = *((uint32_t*) (buffer + ipHeaderOffset + 16));
				printf("sh.ipaddr2 = %i\n", sh.ipaddr2);
				sh.r2 = *((uint32_t*) (buffer + ipHeaderOffset + 20));
				printf("sh.r2 = %i\n", sh.r2);
				sh.ipaddr3 = *((uint32_t*) (buffer + ipHeaderOffset + 24));
				printf("sh.ipaddr3 = %i\n", sh.ipaddr3);
				sh.r3 = *((uint32_t*) (buffer + ipHeaderOffset + 28));
				printf("sh.r3 = %i\n", sh.r3);
				sh.ipaddr4 = *((uint32_t*) (buffer + ipHeaderOffset + 32));
				printf("sh.ipaddr4 = %i\n", sh.ipaddr4);
				sh.r4 = *((uint32_t*) (buffer + ipHeaderOffset + 36));
				printf("sh.r4 = %i\n", sh.r4);
				sh.ipaddr5 = *((uint32_t*) (buffer + ipHeaderOffset + 40));
				printf("sh.ipaddr5 = %i\n", sh.ipaddr5);
				sh.r5 = *((uint32_t*) (buffer + ipHeaderOffset + 44));
				printf("sh.r5 = %i\n", sh.r5);
				sh.ipaddr6 = *((uint32_t*) (buffer + ipHeaderOffset + 48));
				printf("sh.ipaddr6 = %i\n", sh.ipaddr6);
				sh.r6 = *((uint32_t*) (buffer + ipHeaderOffset + 52));
				printf("sh.r6 = %i\n", sh.r6);
		
				printf("///////////////////////////////////\n");
		
				// update shim
				if (sh.size > 0) {
					sh.size--;
			
					switch(sh.offset) {
					case 2:
						inet_pton(AF_INET, ipAddress, &sh.ipaddr2);
						sh.r2 = 2;
						break;
					case 3:
						inet_pton(AF_INET, ipAddress, &sh.ipaddr3);
						sh.r3 = 3;
						break;
					case 4:
						inet_pton(AF_INET, ipAddress, &sh.ipaddr4);
						sh.r4 = 4;
						break;
					case 5:
						inet_pton(AF_INET, ipAddress, &sh.ipaddr5);
						sh.r5 = 5;
						break;
					case 6:
						inet_pton(AF_INET, ipAddress, &sh.ipaddr6);
						sh.r6 = 6;
						break;
					}
			
					sh.offset++;
				} else {
					// shim is full
					printf("Shim is full, dropping packet..\n");
					
					return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);
				}
				
				buf = (unsigned char*)malloc(ret);
				// ip header
				memcpy(buf, buffer, ipHeaderOffset);
				memcpy((buf + ipHeaderOffset), &sh, 56);
				memcpy((buf + ipHeaderOffset + 56), (buffer + ipHeaderOffset), (ret-ipHeaderOffset));
				printPacket(buf);
		
				return nfq_set_verdict(qh, id, NF_ACCEPT, ret, buf);
			} else if (fromHost && TTL == 63) {
				// From one of your hosts, (1 hop)
				// Create new shim
				
				// Add shim
				printf("Adding shim header...\n");
				// Normal traffic
				sh.type = normal_shim_type;
				// Start at 5 since going to fill self
				sh.size = 5;
				// offset starts at 2 since filling self
				sh.offset = 2;
				// generate nonce
				sh.nonce = 4444;
				inet_pton(AF_INET, ipAddress, &sh.ipaddr1);
				sh.r1 = 1;
				sh.ipaddr2 = 0;
				sh.r2 = 0;
				sh.ipaddr3 = 0;
				sh.r3 = 0;
				sh.ipaddr4 = 0;
				sh.r4 = 0;
				sh.ipaddr5 = 0;
				sh.r5 = 0;
				sh.ipaddr6 = 0;
				sh.r6 = 0;
		
				buf = (unsigned char*)malloc(ret);
				// ip header
				memcpy(buf, buffer, ipHeaderOffset);
				memcpy((buf + ipHeaderOffset), &sh, 56);
				memcpy((buf + ipHeaderOffset + 56), (buffer + 56 +  ipHeaderOffset), (ret-ipHeaderOffset-56));
				printPacket(buf);
		
				return nfq_set_verdict(qh, id, NF_ACCEPT, ret, buf);
			} else if (toLegacyHost) {
				// Strip shim
				printf("Stripping Shim\n");
				
				buf = (unsigned char*)malloc(ret);
				memcpy(buf, buffer, ipHeaderOffset);
				memcpy((buf + ipHeaderOffset), (buffer + ipHeaderOffset + 56), (ret-ipHeaderOffset));
				
				return nfq_set_verdict(qh, id, NF_ACCEPT, ret, buf);
			}
		}
		
	}
	
	printf("Allow packet\n");
	return nfq_set_verdict(qh, id, NF_ACCEPT, 0, NULL);
}

void printPacket(unsigned char* buffer) {
	uint32_t ip_src, ip_dest;
	struct in_addr s_ip, d_ip;
	struct Shim_Header sh;
	int ipHeaderOffset = 20;

	ip_src = *((uint32_t*) (buffer + 12));
	s_ip.s_addr = (uint32_t) ip_src;
	ip_dest = *((uint32_t*) (buffer + 16));
	d_ip.s_addr = (uint32_t) ip_dest;
	
	printf("******************************\n");
	printf("Printing Packet from Buffer\n\n");
	
	printf("IP Header Info\n");
	printf("Source IP: %s\n", inet_ntoa(s_ip));
	printf("Destination IP: %s\n\n", inet_ntoa(d_ip));
	
	printf("Shim Header Info\n");
	sh.type = *((uint8_t*) (buffer + ipHeaderOffset));
	printf("sh.type = %i\n", sh.type);
	sh.size = *((uint8_t*) (buffer + ipHeaderOffset + 1));
	printf("sh.size = %i\n", sh.size);
	sh.offset = *((uint16_t*) (buffer + ipHeaderOffset + 2));
	printf("sh.offset = %i\n", sh.offset);
	sh.nonce = *((uint32_t*) (buffer + ipHeaderOffset + 4));
	printf("sh.nonce = %i\n", sh.nonce);
	sh.ipaddr1 = *((uint32_t*) (buffer + ipHeaderOffset + 8));
	printf("sh.ipaddr1 = %i\n", sh.ipaddr1);
	sh.r1 = *((uint32_t*) (buffer + ipHeaderOffset + 12));
	printf("sh.r1 = %i\n", sh.r1);
	sh.ipaddr2 = *((uint32_t*) (buffer + ipHeaderOffset + 16));
	printf("sh.ipaddr2 = %i\n", sh.ipaddr2);
	sh.r2 = *((uint32_t*) (buffer + ipHeaderOffset + 20));
	printf("sh.r2 = %i\n", sh.r2);
	sh.ipaddr3 = *((uint32_t*) (buffer + ipHeaderOffset + 24));
	printf("sh.ipaddr3 = %i\n", sh.ipaddr3);
	sh.r3 = *((uint32_t*) (buffer + ipHeaderOffset + 28));
	printf("sh.r3 = %i\n", sh.r3);
	sh.ipaddr4 = *((uint32_t*) (buffer + ipHeaderOffset + 32));
	printf("sh.ipaddr4 = %i\n", sh.ipaddr4);
	sh.r4 = *((uint32_t*) (buffer + ipHeaderOffset + 36));
	printf("sh.r4 = %i\n", sh.r4);
	sh.ipaddr5 = *((uint32_t*) (buffer + ipHeaderOffset + 40));
	printf("sh.ipaddr5 = %i\n", sh.ipaddr5);
	sh.r5 = *((uint32_t*) (buffer + ipHeaderOffset + 44));
	printf("sh.r5 = %i\n", sh.r5);
	sh.ipaddr6 = *((uint32_t*) (buffer + ipHeaderOffset + 48));
	printf("sh.ipaddr6 = %i\n", sh.ipaddr6);
	sh.r6 = *((uint32_t*) (buffer + ipHeaderOffset + 52));
	printf("sh.r6 = %i\n", sh.r6);
	
	printf("******************************\n");
}

int main(int argc, char **argv)
{
	struct nfq_handle *h;
	struct nfq_q_handle *qh;
	struct nfnl_handle *nh;
	int fd;
	int rv;
	char buf[4096] __attribute__ ((aligned));
	
	// Keep track of hosts on network
	struct Host_Info legacy = {
		.host_id = id_legacy,
		.isLegacy = true
	};

	struct Host_Info attacker = {
		.host_id = id_attacker,
		.isLegacy = false
	};

	struct Host_Info victim = {
		.host_id = id_victim,
		.isLegacy = false
	};
	
	strcpy(legacy.ip,"169.254.9.184");
	hostTable[0] = legacy;
	strcpy(attacker.ip,"169.254.12.172");
	hostTable[1] = attacker;
	strcpy(victim.ip,"169.254.7.37");
	hostTable[2] = victim;

	//printf("opening library handle\n");
	h = nfq_open();
	if (!h) {
		fprintf(stderr, "error during nfq_open()\n");
		exit(1);
	}

	//printf("unbinding existing nf_queue handler for AF_INET (if any)\n");
	if (nfq_unbind_pf(h, AF_INET) < 0) {
		fprintf(stderr, "error during nfq_unbind_pf()\n");
		exit(1);
	}

	//printf("binding nfnetlink_queue as nf_queue handler for AF_INET\n");
	if (nfq_bind_pf(h, AF_INET) < 0) {
		fprintf(stderr, "error during nfq_bind_pf()\n");
		exit(1);
	}

	//printf("binding this socket to queue '0'\n");
	qh = nfq_create_queue(h,  0, &cb, NULL);
	if (!qh) {
		fprintf(stderr, "error during nfq_create_queue()\n");
		exit(1);
	}

	//printf("setting copy_packet mode\n");
	if (nfq_set_mode(qh, NFQNL_COPY_PACKET, 0xffff) < 0) {
		fprintf(stderr, "can't set packet_copy mode\n");
		exit(1);
	}

	fd = nfq_fd(h);

	for (;;) {
		if ((rv = recv(fd, buf, sizeof(buf), 0)) >= 0) {
			printf("pkt received\n");
			nfq_handle_packet(h, buf, rv);
			continue;
		}
		/* if your application is too slow to digest the packets that
		 * are sent from kernel-space, the socket buffer that we use
		 * to enqueue packets may fill up returning ENOBUFS. Depending
		 * on your application, this error may be ignored. Please, see
		 * the doxygen documentation of this library on how to improve
		 * this situation.
		 */
		if (rv < 0 && errno == ENOBUFS) {
			printf("losing packets!\n");
			continue;
		}
		perror("recv failed");
		break;
	}

	printf("unbinding from queue 0\n");
	nfq_destroy_queue(qh);

#ifdef INSANE
	/* normally, applications SHOULD NOT issue this command, since
	 * it detaches other programs/sockets from AF_INET, too ! */
	printf("unbinding from AF_INET\n");
	nfq_unbind_pf(h, AF_INET);
#endif

	printf("closing library handle\n");
	nfq_close(h);

	exit(0);
}
