#ifndef GATEWAY_H
#define GATEWAY_H

#define BUFFER 1444
#define PREROUTING 0
#define POSTROUTING 4

typedef struct Shim_Header {
	uint8_t type;
	uint8_t size;
	uint16_t offset;
	uint32_t nonce;
	uint32_t ipaddr1;
	uint32_t r1;
	uint32_t ipaddr2;
	uint32_t r2;
	uint32_t ipaddr3;
	uint32_t r3;
	uint32_t ipaddr4;
	uint32_t r4;
	uint32_t ipaddr5;
	uint32_t r5;
	uint32_t ipaddr6;
	uint32_t r6;
} Shim_Header;

typedef struct Host_Info {
	// Host_Type
	int host_id;
	// Is the host legacy?
	bool isLegacy;
	char ip[14];
} Host_Info;

enum Host_Id {
	id_legacy,
	id_victim,
	id_attacker
};

enum Shim_Type {
	// Normal traffic
	normal_shim_type,
	// Filter request
	filter_request_shim_type,
	// Handshake
	handshake_shim_type,
	// Bad traffic
	bad_traffic_shim_type
};

// Prototypes
void printPacket(unsigned char* buffer);

#endif
