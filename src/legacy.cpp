/*
	dstapply
	paleondires
	
	AITF Host Client
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include "host.h"

int main() {
	using namespace std;
	
	int sockfd, newfd;
	const char* port = "4404";
	struct addrinfo hints, *contrInfo, *p;
	struct sockaddr_storage their_addr;
	socklen_t sin_size;
	int yes = 1;
	char ip[INET_ADDRSTRLEN];
	int rv;
	int numbytes = 0;
	char buf[BUFFER];
	
	memset(buf, '\0', BUFFER);
	
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	printf("Starting AITF Host Client\n");
	
	if ((rv = getaddrinfo(NULL, port, &hints, &contrInfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	// loop through all the results and bind to the first we can
	for(p = contrInfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
				p->ai_protocol)) == -1) {
			perror("Host: socket");
			continue;
		}

		if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
				sizeof(int)) == -1) {
			perror("setsockopt");
			exit(1);
		}

		if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("Host: bind");
			continue;
		}

		break;
	}
	
	if (p == NULL)  {
		fprintf(stderr, "Host: failed to bind\n");
		return 2;
	}

	// contrInfo not needed anymore
	freeaddrinfo(contrInfo);
	
	if (listen(sockfd, 10) == -1) {
			perror("listen");
			exit(1);
	}

	printf("Host: waiting for connections...\n");
	
	while (true) {
		sin_size = sizeof(their_addr);
		newfd = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);
		if (newfd == -1) {
			perror("accept");
			continue;
		}

		inet_ntop(their_addr.ss_family,
			get_in_addr((struct sockaddr *)&their_addr),
			ip, sizeof(ip));
		printf("Host: got connection from %s\n", ip);

		// child process
		if (!fork()) { 
			// child doesn't need listener
			close(sockfd);
			
			// Child Fields
			struct Shim_Header receivedShim;
			struct Shim_Header sendShim;
			char* saveptr;
			
			memset(buf, '\0', BUFFER);
			
			// Initial auth packet
			if (!(numbytes = recv(newfd, buf, BUFFER, 0))) {
				perror("Failed to receive packet (blocking)");
			}
			
			// Send packet
			//if (send(newfd, &sendPacket, sizeof(sendPacket), 0) == -1)
				//perror("send request failed");
				
			while (true) {
				memset(buf, '\0', BUFFER);
			
				if ((numbytes = recv(newfd, buf, BUFFER, MSG_DONTWAIT)) >= 0) {
					printf("pkt received\n");
					
					// Handle Packet
					
				}
			}
			close(newfd);
			exit(0);
		}
	}
	
	// parent doesn't need this
	close(newfd);
	
	return 0;
}

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa) {
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

