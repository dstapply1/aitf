/*
	dstapply
	paleondires
	
	AITF Host Client
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include "host.h"

int main(int argc, char* argv[]) {
	using namespace std;
	
	int sockfd, newfd;
	struct in_addr addr;
	const char* victimIP = "169.254.7.37";
	inet_aton(victimIP, &addr);
	const char* port = "4404";
	struct addrinfo hints, *contrInfo, *p;
	struct sockaddr_storage their_addr;
	socklen_t sin_size;
	int yes = 1;
	char ip[INET_ADDRSTRLEN];
	int rv;
	int numbytes = 0;
	char buf[BUFFER];
	bool attackMode = false;
	
	memset(buf, '\0', BUFFER);
	
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	
	// Handle arguments
	if (argc == 2) {
		// Can Skip program name
		for (int i = 1; i < argc; i++) {
			if (!strcmp(argv[i], "--mode=attack")) {
				attackMode = true;
			}
		}
	}

	printf("Starting AITF Host Client\n");
	
	if ((rv = getaddrinfo(victimIP, port, &hints, &contrInfo)) != 0) {
    	fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
    	return 1;
    }
	
	// loop through all the results and connect to the first we can
	for (p = contrInfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
				p->ai_protocol)) == -1) {
			perror("Attacker: socket");
			continue;
		}

		if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("Attacker: connect");
			continue;
		}

		break;
	}

	// Didnt connect to client
	if (p == NULL) {
		fprintf(stderr, "Attacker: failed to connect\n");
		return 2;
	}
	
	// translate ip address to human readable
	inet_ntop(p->ai_family, get_in_addr((struct sockaddr *)p->ai_addr),
			ip, sizeof(ip));
	printf("Attacker: connecting to %s\n", ip);
	
	// Dont need this anymore
	freeaddrinfo(contrInfo);
	
	memset(buf, 't', BUFFER-1);
	
	// Send auth packet
	if (send(sockfd, buf, BUFFER, 0) == -1)
		perror("send request failed");
		
	memset(buf, '\0', BUFFER);
		
	while (true) {
	
		if (attackMode) {
			memset(buf, 't', BUFFER-1);
			
			if (send(sockfd, buf, BUFFER, 0) == -1)
				perror("send request failed");
				
			memset(buf, 't', BUFFER-1);
		}
		
		if ((numbytes = recv(sockfd, buf, BUFFER, MSG_DONTWAIT)) >= 0) {
			printf("pkt received\n");
			
			if (attackMode) {
				//Check if request to stop sending traffic
			}
			
			memset(buf, '\0', BUFFER);
		}
	}
	
	// parent doesn't need this
	close(sockfd);
	
	return 0;
}

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa) {
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

