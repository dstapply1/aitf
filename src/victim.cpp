/*
	dstapply
	paleondires
	
	AITF Host Client
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <linux/types.h>
#include <linux/netfilter.h>		/* for NF_ACCEPT */
#include "host.h"

#include <libnetfilter_queue/libnetfilter_queue.h>

/* returns packet id */
static u_int32_t print_pkt (struct nfq_data *tb)
{
	int id = 0;
	struct nfqnl_msg_packet_hdr *ph;
	struct nfqnl_msg_packet_hw *hwph;
	u_int32_t mark,ifi; 
	int ret;
	unsigned char *data;
	uint32_t ip_src, ip_dest;
	struct in_addr s_ip, d_ip;

	ph = nfq_get_msg_packet_hdr(tb);
	if (ph) {
		id = ntohl(ph->packet_id);
		printf("hw_protocol=0x%04x hook=%u id=%u ",
			ntohs(ph->hw_protocol), ph->hook, id);
	}

	hwph = nfq_get_packet_hw(tb);
	if (hwph) {
		int i, hlen = ntohs(hwph->hw_addrlen);

		printf("hw_src_addr=");
		for (i = 0; i < hlen-1; i++)
			printf("%02x:", hwph->hw_addr[i]);
		printf("%02x ", hwph->hw_addr[hlen-1]);
	}

	mark = nfq_get_nfmark(tb);
	if (mark)
		printf("mark=%u ", mark);

	ifi = nfq_get_indev(tb);
	if (ifi)
		printf("indev=%u ", ifi);

	ifi = nfq_get_outdev(tb);
	if (ifi)
		printf("outdev=%u ", ifi);
	ifi = nfq_get_physindev(tb);
	if (ifi)
		printf("physindev=%u ", ifi);

	ifi = nfq_get_physoutdev(tb);
	if (ifi)
		printf("physoutdev=%u ", ifi);

	ret = nfq_get_payload(tb, &data);
	if (ret >= 0) {
		printf("payload_len=%d ", ret);
		ip_src = *((uint32_t*) (data + 12));
		s_ip.s_addr = (uint32_t) ip_src;
		printf(" source IP %s", inet_ntoa(s_ip));
		ip_dest = *((uint32_t*) (data + 16));
		d_ip.s_addr = (uint32_t) ip_dest;
		printf(" destination IP %s\n", inet_ntoa(d_ip));
	}
	
	fputc('\n', stdout);

	return id;
}

static int cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg,
	      struct nfq_data *nfa, void *data)
{
	u_int32_t id = print_pkt(nfa);
	printf("entering callback\n");
	
	struct Shim_Header sh;
	unsigned char *buffer;
	uint32_t ip_src, ip_dest;
	struct in_addr s_ip, d_ip;
	bool fromHost = false;
	bool toLegacyHost = false;
	unsigned char* buf = NULL;
	void* bufferpointer;
	void* bufferpointer2;
	int ipHeaderOffset = 20;
	
	int ret = nfq_get_payload(nfa, &buffer);
	if (ret >= 0) {
		ip_src = *((uint32_t*) (buffer + 12));
		s_ip.s_addr = (uint32_t) ip_src;
		ip_dest = *((uint32_t*) (buffer + 16));
		d_ip.s_addr = (uint32_t) ip_dest;
		
		// Handle shim actions (requests etc)
		if (ret > 76) {
			// Read shim header
			printf("///////////////////////////////////\n");
			printf("Reading shim header...\n");
		
			printf("Shim Header Info\n");
			sh.type = *((uint8_t*) (buffer + ipHeaderOffset));
			printf("sh.type = %i\n", sh.type);
			sh.size = *((uint8_t*) (buffer + ipHeaderOffset + 1));
			printf("sh.size = %i\n", sh.size);
			sh.offset = *((uint16_t*) (buffer + ipHeaderOffset + 2));
			printf("sh.offset = %i\n", sh.offset);
			sh.nonce = *((uint32_t*) (buffer + ipHeaderOffset + 4));
			printf("sh.nonce = %i\n", sh.nonce);
			sh.ipaddr1 = *((uint32_t*) (buffer + ipHeaderOffset + 8));
			printf("sh.ipaddr1 = %i\n", sh.ipaddr1);
			sh.r1 = *((uint32_t*) (buffer + ipHeaderOffset + 12));
			printf("sh.r1 = %i\n", sh.r1);
			sh.ipaddr2 = *((uint32_t*) (buffer + ipHeaderOffset + 16));
			printf("sh.ipaddr2 = %i\n", sh.ipaddr2);
			sh.r2 = *((uint32_t*) (buffer + ipHeaderOffset + 20));
			printf("sh.r2 = %i\n", sh.r2);
			sh.ipaddr3 = *((uint32_t*) (buffer + ipHeaderOffset + 24));
			printf("sh.ipaddr3 = %i\n", sh.ipaddr3);
			sh.r3 = *((uint32_t*) (buffer + ipHeaderOffset + 28));
			printf("sh.r3 = %i\n", sh.r3);
			sh.ipaddr4 = *((uint32_t*) (buffer + ipHeaderOffset + 32));
			printf("sh.ipaddr4 = %i\n", sh.ipaddr4);
			sh.r4 = *((uint32_t*) (buffer + ipHeaderOffset + 36));
			printf("sh.r4 = %i\n", sh.r4);
			sh.ipaddr5 = *((uint32_t*) (buffer + ipHeaderOffset + 40));
			printf("sh.ipaddr5 = %i\n", sh.ipaddr5);
			sh.r5 = *((uint32_t*) (buffer + ipHeaderOffset + 44));
			printf("sh.r5 = %i\n", sh.r5);
			sh.ipaddr6 = *((uint32_t*) (buffer + ipHeaderOffset + 48));
			printf("sh.ipaddr6 = %i\n", sh.ipaddr6);
			sh.r6 = *((uint32_t*) (buffer + ipHeaderOffset + 52));
			printf("sh.r6 = %i\n", sh.r6);
		
			printf("///////////////////////////////////\n");
		
			// Strip shim
			printf("Stripping Shim\n");

			buf = (unsigned char*)malloc(ret);
			memcpy(buf, buffer, ipHeaderOffset);
			memcpy((buf + ipHeaderOffset), (buffer + ipHeaderOffset + 56), (ret-(56+ipHeaderOffset)));
		

			ip_src = *((uint32_t*) (buf + 12));
			s_ip.s_addr = (uint32_t) ip_src;
			ip_dest = *((uint32_t*) (buf + 16));
			d_ip.s_addr = (uint32_t) ip_dest;
	
			printf("******************************\n");
			printf("Printing Packet from Buffer\n\n");
	
			printf("IP Header Info\n");
			printf("Source IP: %s\n", inet_ntoa(s_ip));
			printf("Destination IP: %s\n\n", inet_ntoa(d_ip));
		
			return nfq_set_verdict(qh, id, NF_ACCEPT, ret, buf);
		}
	}
	
	printf("Setting verdict...\n");
	return nfq_set_verdict(qh, id, NF_ACCEPT, 0, NULL);
}

void printPacket(unsigned char* buffer) {
	uint32_t ip_src, ip_dest;
	struct in_addr s_ip, d_ip;
	struct Shim_Header sh;
	int ipHeaderOffset = 20;

	ip_src = *((uint32_t*) (buffer + 12));
	s_ip.s_addr = (uint32_t) ip_src;
	ip_dest = *((uint32_t*) (buffer + 16));
	d_ip.s_addr = (uint32_t) ip_dest;
	
	printf("******************************\n");
	printf("Printing Packet from Buffer\n\n");
	
	printf("IP Header Info\n");
	printf("Source IP: %s\n", inet_ntoa(s_ip));
	printf("Destination IP: %s\n\n", inet_ntoa(d_ip));
	
	printf("Shim Header Info\n");
	sh.type = *((uint8_t*) (buffer + ipHeaderOffset));
	printf("sh.type = %i\n", sh.type);
	sh.size = *((uint8_t*) (buffer + ipHeaderOffset + 1));
	printf("sh.size = %i\n", sh.size);
	sh.offset = *((uint16_t*) (buffer + ipHeaderOffset + 2));
	printf("sh.offset = %i\n", sh.offset);
	sh.nonce = *((uint32_t*) (buffer + ipHeaderOffset + 4));
	printf("sh.nonce = %i\n", sh.nonce);
	sh.ipaddr1 = *((uint32_t*) (buffer + ipHeaderOffset + 8));
	printf("sh.ipaddr1 = %i\n", sh.ipaddr1);
	sh.r1 = *((uint32_t*) (buffer + ipHeaderOffset + 12));
	printf("sh.r1 = %i\n", sh.r1);
	sh.ipaddr2 = *((uint32_t*) (buffer + ipHeaderOffset + 16));
	printf("sh.ipaddr2 = %i\n", sh.ipaddr2);
	sh.r2 = *((uint32_t*) (buffer + ipHeaderOffset + 20));
	printf("sh.r2 = %i\n", sh.r2);
	sh.ipaddr3 = *((uint32_t*) (buffer + ipHeaderOffset + 24));
	printf("sh.ipaddr3 = %i\n", sh.ipaddr3);
	sh.r3 = *((uint32_t*) (buffer + ipHeaderOffset + 28));
	printf("sh.r3 = %i\n", sh.r3);
	sh.ipaddr4 = *((uint32_t*) (buffer + ipHeaderOffset + 32));
	printf("sh.ipaddr4 = %i\n", sh.ipaddr4);
	sh.r4 = *((uint32_t*) (buffer + ipHeaderOffset + 36));
	printf("sh.r4 = %i\n", sh.r4);
	sh.ipaddr5 = *((uint32_t*) (buffer + ipHeaderOffset + 40));
	printf("sh.ipaddr5 = %i\n", sh.ipaddr5);
	sh.r5 = *((uint32_t*) (buffer + ipHeaderOffset + 44));
	printf("sh.r5 = %i\n", sh.r5);
	sh.ipaddr6 = *((uint32_t*) (buffer + ipHeaderOffset + 48));
	printf("sh.ipaddr6 = %i\n", sh.ipaddr6);
	sh.r6 = *((uint32_t*) (buffer + ipHeaderOffset + 52));
	printf("sh.r6 = %i\n", sh.r6);
	
	printf("******************************\n");
}

int main() {
	using namespace std;
	// Normal Socket stuff
	int sockfd, newfd;
	const char* port = "4404";
	struct addrinfo hints, *contrInfo, *p;
	struct sockaddr_storage their_addr;
	socklen_t sin_size;
	int yes = 1;
	char ip[INET_ADDRSTRLEN];
	int rv;
	int numbytes = 0;
	char recvbuf[BUFFER];
	
	// Netfilter stuff
	struct nfq_handle *h;
	struct nfq_q_handle *qh;
	struct nfnl_handle *nh;
	int fd;
	int rv2;
	char buf[4096] __attribute__ ((aligned));
	int ipHeaderOffset = 20;
	
	memset(recvbuf, '\0', BUFFER);
	
	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;

	printf("Starting AITF Host Client\n");
	
	if ((rv = getaddrinfo(NULL, port, &hints, &contrInfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}

	// loop through all the results and bind to the first we can
	for(p = contrInfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
				p->ai_protocol)) == -1) {
			perror("Host: socket");
			continue;
		}

		if (setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &yes,
				sizeof(int)) == -1) {
			perror("setsockopt");
			exit(1);
		}

		if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("Host: bind");
			continue;
		}

		break;
	}
	
	if (p == NULL)  {
		fprintf(stderr, "Host: failed to bind\n");
		return 2;
	}

	// contrInfo not needed anymore
	freeaddrinfo(contrInfo);
	
	if (!fork()) {
	
		if (listen(sockfd, 10) == -1) {
				perror("listen");
				exit(1);
		}

		printf("Host: waiting for connections...\n");
	
		while (true) {
			sin_size = sizeof(their_addr);
			newfd = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);
			if (newfd == -1) {
				perror("accept");
				continue;
			}

			inet_ntop(their_addr.ss_family,
				get_in_addr((struct sockaddr *)&their_addr),
				ip, sizeof(ip));
			printf("Host: got connection from %s\n", ip);

			// child process
			if (!fork()) { 
				// child doesn't need listener
				close(sockfd);
			
				// Child Fields
				struct Shim_Header receivedShim;
				struct Shim_Header sendShim;
				char* saveptr;
			
				memset(recvbuf, '\0', BUFFER);
			
				// Initial auth packet
				if (!(numbytes = recv(newfd, recvbuf, BUFFER, 0))) {
					perror("Failed to receive packet (blocking)");
				}
			
				// Test Packet
				memset(recvbuf, 't', BUFFER-1);
			
				// Send packet
				if (send(newfd, recvbuf, BUFFER, 0) == -1)
					perror("send request failed");
				
				memset(recvbuf, '\0', BUFFER);
				
				while (true) {
				
			
					if ((numbytes = recv(newfd, recvbuf, BUFFER, MSG_DONTWAIT)) >= 0) {
						printf("pkt received\n");
					
						// Handle Packet
					
						// reset recvbuf
						memset(recvbuf, '\0', BUFFER);
					}
				}
				close(newfd);
				exit(0);
			}
		}
	
		// parent doesn't need this
		close(sockfd);
		exit(0);
	} //end of main fork
	
	close(sockfd);
	
	// Grab incoming packets to handle header
	
	//printf("opening library handle\n");
	h = nfq_open();
	if (!h) {
		fprintf(stderr, "error during nfq_open()\n");
		exit(1);
	}

	//printf("unbinding existing nf_queue handler for AF_INET (if any)\n");
	if (nfq_unbind_pf(h, AF_INET) < 0) {
		fprintf(stderr, "error during nfq_unbind_pf()\n");
		exit(1);
	}

	//printf("binding nfnetlink_queue as nf_queue handler for AF_INET\n");
	if (nfq_bind_pf(h, AF_INET) < 0) {
		fprintf(stderr, "error during nfq_bind_pf()\n");
		exit(1);
	}

	//printf("binding this socket to queue '0'\n");
	qh = nfq_create_queue(h,  0, &cb, NULL);
	if (!qh) {
		fprintf(stderr, "error during nfq_create_queue()\n");
		exit(1);
	}

	//printf("setting copy_packet mode\n");
	if (nfq_set_mode(qh, NFQNL_COPY_PACKET, 0xffff) < 0) {
		fprintf(stderr, "can't set packet_copy mode\n");
		exit(1);
	}

	fd = nfq_fd(h);

	for (;;) {
		if ((rv2 = recv(fd, buf, sizeof(buf), 0)) >= 0) {
			printf("pkt received\n");
			nfq_handle_packet(h, buf, rv2);
			continue;
		}
		/* if your application is too slow to digest the packets that
		 * are sent from kernel-space, the socket buffer that we use
		 * to enqueue packets may fill up returning ENOBUFS. Depending
		 * on your application, this error may be ignored. Please, see
		 * the doxygen documentation of this library on how to improve
		 * this situation.
		 */
		if (rv2 < 0 && errno == ENOBUFS) {
			printf("losing packets!\n");
			continue;
		}
		perror("recv failed");
		break;
	}

	printf("unbinding from queue 0\n");
	nfq_destroy_queue(qh);

#ifdef INSANE
	/* normally, applications SHOULD NOT issue this command, since
	 * it detaches other programs/sockets from AF_INET, too ! */
	printf("unbinding from AF_INET\n");
	nfq_unbind_pf(h, AF_INET);
#endif

	printf("closing library handle\n");
	nfq_close(h);
	
	return 0;
}

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa) {
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}

	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

