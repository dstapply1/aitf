#ifndef HOST_H
#define HOST_H

// 1500 MTU - shim header size
#define BUFFER 1444

typedef struct Shim_Header {
	uint8_t type;
	uint8_t size;
	uint16_t offset;
	uint32_t nonce;
	uint32_t ipaddr1;
	uint32_t r1;
	uint32_t ipaddr2;
	uint32_t r2;
	uint32_t ipaddr3;
	uint32_t r3;
	uint32_t ipaddr4;
	uint32_t r4;
	uint32_t ipaddr5;
	uint32_t r5;
	uint32_t ipaddr6;
	uint32_t r6;
} shim_header;

enum Shim_Type {
	// Normal traffic
	normal_shim_type,
	// Filter request
	filter_request_shim_type,
	// Handshake
	handshake_shim_type,
	// Bad traffic
	bad_traffic_shim_type
};

// Prototypes
void *get_in_addr(struct sockaddr *sa);

#endif
